// Hàm getElement byID

function getID(eID) {
  return document.getElementById(eID);
}

/**
 * Bài 1
 */
function timN() {
  var n = 1;
  var tong = 0;
  do {
    n++;
    tong = tong + n;
  } while (tong < 10000);
  getID("soN").innerText = `Số nguyên dương nhỏ nhất: ${n}`;
}

/**
 * Bài 2
 */

var x, n, S;
function tinhTong() {
  x = getID("soX").value * 1;
  n = getID("soMu").value * 1;
  S = 0;
  if (n < 0) {
    alert("Vui lòng nhập số mũ N>=0");
    getID("tongS").innerText = "";
  } else {
    for (var i = 1; i <= n; i++) {
      S = S + Math.pow(x, i);
    }
    getID("tongS").innerText = `Tổng: ${S}`;
  }
}
/**
 * Bài 3
 */
var sN, giaiThua;
function tinhGiaithua() {
  sN = getID("sN").value * 1;
  giaiThua = 1;
  if (sN < 0) {
    alert("Dữ liệu không hợp lệ! Vui lòng kiểm tra lại.");
    getID("giaiThua").innerText = "";
  } else if (sN == 0) {
    getID("giaiThua").innerText = `Giai thừa: ${giaiThua}`;
  } else {
    for (var k = 1; k <= sN; k++) {
      giaiThua = giaiThua * k;
    }
    getID("giaiThua").innerText = `Giai thừa: ${giaiThua}`;
  }
}

/**
 * Bài 4
 */

function inDiv() {
  var chuoiDiv = "";
  for (var i = 1; i < 11; i++) {
    if (i % 2 == 0) {
      chuoiDiv += `<div class="bg-danger text-white p-2">Div chẳn ${i}</div>`;
    } else {
      chuoiDiv += `<div class="bg-primary text-white p-2">Div lẻ ${i}</div>`;
    }
  }
  getID("inThediv").innerHTML = chuoiDiv;
}
